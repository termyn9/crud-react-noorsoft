import React from 'react';
import { connect } from 'react-redux';
import {addPost } from '../../redux/action-creator'
class AddNewPost extends React.Component{
 state={data:''}
 emailInput = React.createRef() //createRef() для возможности ссылаться на 
 ageInput = React.createRef() //экземпляр класса в любой части компонента

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });

  };
   handleSubmit = (event) => {
    this.props.addPost(this.state);   
     this.emailInput.current.value='';
    this.ageInput.current.value=''; 
        event.preventDefault();
                            
  };

 render(){
  return(
   <form onSubmit={ this.handleSubmit } className=' mb-3 form-group'>
   <input ref={this.emailInput} name="Email" type="text" className="form-control  col-lg-2 mb-3 mt-3" maxLength='50' placeholder="email"  required value={this.state.data.Email} onChange={this.handleChange}/>
   <input ref={this.ageInput} name="Age" type="text" className="form-control col-lg-2 mb-3 mt-3"  maxLength='50' placeholder="age"  required value={this.state.data.Age} onChange={this.handleChange}/>
   <button className="btn btn-info mb-6" type="submit"> Add post</button>
   </form >
  )
 }
}
const mapDispatchToProps = { addPost };                     
export default connect(null, mapDispatchToProps)(AddNewPost);  
