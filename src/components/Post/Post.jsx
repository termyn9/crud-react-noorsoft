import React from "react";
import { connect } from "react-redux";
import { deletePost ,updatePost} from "../../redux/action-creator";
class Post extends React.Component {
  constructor(props) {
  super(props);
  this.handleEmailChange = this.handleEmailChange.bind(this);
  this.handleAgeChange = this.handleAgeChange.bind(this);
  this.handleSubmit = this.handleSubmit.bind(this);

  this.state = {
    id :this.props.id,
    email: this.props.email,
    age: this.props.age,
    
    readOnly:true,
  }
}
 handleEmailChange(event) {
  console.log('handleEmailChange', this);
  this.setState({email: event.target.value});
}
 handleAgeChange(event) {
  console.log('handleEmailChange', this);
  this.setState({age: event.target.value});
}
handleSubmit(event) {
  event.preventDefault();
    const id = this.props.id;
    const email = this.state.email ? this.state.email : this.props.email;
    const age = this.state.age ? this.state.age : this.props.age;
    const post = {_id: id, data:{Email: email, Age : age}} 
    this.props.updatePost(post);
}
  editPostHandler = (event) => {
    let editBtn = document.querySelectorAll(".js-edit");
    editBtn.forEach((btn) => {
      if ((btn = event.target)) {
        btn.style.display = "none";
      }
    });
    this.setState({ readOnly: (this.state.readOnly = false) });
  };
  savePostHandler = (event) => {
    event.preventDefault();
    let saveBtn = document.querySelectorAll(".js-save");
    saveBtn.forEach((btn) => {
      if ((btn = event.target)) {
        btn.style.display = "none";
      }
    });
    this.setState({ readOnly: (this.state.readOnly = true) });
    this.handleSubmit(event);
  };
 

  render() {
    return (
      <tr>
        <td>
          <input
            className="form-control"
            readOnly={this.state.readOnly}
            value={this.props.id}
          />
        </td>
        <td>
          <input
            name="email"
            type="text"
            className="form-control"
            readOnly={this.state.readOnly}
            value={this.state.email}
            form="myform"
            onChange={this.handleEmailChange}
          />
        </td>
        <td>
          <input
            name="age"
            type="numder"
            form="myform"
            className="form-control"
            readOnly={this.state.readOnly}
            value={this.state.age}
            onChange={this.handleAgeChange}
          />
        </td>
        <td>
          <div className="btn-toolbar pull-right">
            {this.state.readOnly === true ? (
              <button
                className="btn btn-primary js-edit"
                onClick={this.editPostHandler.bind(this)}
              > Edit </button>
            ) : null}
            {this.state.readOnly === true ? null : (
              <button
                type="submit"
                className="btn btn-primary js-save"
                onClick={this.savePostHandler.bind(this)}
                form="myform"
              > Save </button>
            )}
             <form method="post" id="myform" onSubmit={this.handleSubmit}></form>
            <button
              onClick={() => this.props.deletePost(this.props.id)}
              className="btn btn-danger"
            > Delete </button>
          </div>
        </td>
      </tr>
    );
  }
}
const mapStateToProps = (state) => ({ post: state.post });
const mapDispatchToProps = { deletePost , updatePost };

export default connect(mapStateToProps, mapDispatchToProps)(Post);
