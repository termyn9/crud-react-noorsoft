import React from 'react';
import Posts from './components/Posts/Posts'
import { connect } from 'react-redux';
import {postFetchData} from './redux/action-creator'

class App extends React.Component{
  
  componentDidMount(){
this.props.fetchData("http://178.128.196.163:3000/api/records")
  }
  render(){
    return (
      <div>
      <Posts posts={this.props.posts}/>
      </div>
    )
  }
}

const mapStateToProps=(state)=>{
  return{
    posts: state.posts
  }
}

const mapDispatchToProps = dispatch => {
  return{
    fetchData:url=>dispatch(postFetchData(url))
  }
}
export default connect (mapStateToProps, mapDispatchToProps)(App);
