import axios from "axios";

export function postFetchDataSuccess(posts) {
  return {
    type: "POSTS_FETCH_DATA_SUCCESS",
    posts
  };
}

export function postFetchData(url) {
  return (dispatch) => {
    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText)
        }
        return response
      })
      .then((response) => response.json())
      .then((posts) => dispatch(postFetchDataSuccess(posts)))
  }
}

export const addPost = ({ Email, Age }) => {
  return (dispatch) => {
    return axios
      .put("http://178.128.196.163:3000/api/records", {
        data: { Email, Age }
      })
      .then((response) => {
        dispatch({
          type: "ADD_POST",
          payload: {
            _id: String(Math.round(Math.random() * 100)),
            data: { Email, Age }
          },
        });
      })
      .catch((error) => {
        throw error;
      });
  };
};

export const deletePost = (id) => {
  return (dispatch) => {
    return axios
      .delete(`http://178.128.196.163:3000/api/records/${id}`)
      .then((response) => {
        dispatch({
          type: "DELETE_POST",
          payload: { id } 
        });
      })
      .catch((error) => {
        throw error;
      });
  };
};

export const updatePost = (post) =>{
  return(dispatch) =>{
    return axios
    .post(`http://178.128.196.163:3000/api/records/${post._id}`,{
      data: { Email: post.data.Email, Age:post.data.Age}
    })
      .then((response) => {
        dispatch({
          type: "UPDATE_POST",
          payload: { id:post._id,  data:{email:post.data.Email, age:post.data.Age}},
        })
      })
         .catch((error) => {
        throw error;
      });
  }
  }
