import React from "react";
import Post from "../Post/Post";
import AddNewPost from "../AddNewPost/AddNewPost";
const Posts = (props) => {
  return (
    <div>
      <AddNewPost />
      <table className="table table-hover">
        <thead>
          <tr>
            <th>Id</th>
            <th>Email</th>
            <th>Age</th>
          </tr>
        </thead>
        <tbody>
          {props.posts.map((post) => (
            <Post id={post._id} email={post.data.Email} age = { post.data.Age }/>
          ))}
        </tbody>
      </table>
    </div>
  );
};
export default Posts;
