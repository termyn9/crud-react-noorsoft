export function posts(state = [], action) {
  
 switch (action.type){
  case "POSTS_FETCH_DATA_SUCCESS":
     return action.posts
   
  case "ADD_POST":                                                    
     return [...state, action.payload]
   
   case "DELETE_POST":
     return state.filter(post => post._id !== action.payload.id)
    //  return state.slice(0, action.payload.id)
    //    .concat(state.slice(action.payload.id + 1))
     
  case "UPDATE_POST":
      return state.map((post) => {
        if (post._id === action.payload.id) {
          debugger;
          return {
            ...post,
            email: action.payload.data.Email,
            age: action.payload.data.Age,
          }
        } else return post
      })
  default: return state
 }
}